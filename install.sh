#!/bin/bash

function get_package_manager() {
  which apk &> /dev/null
  if [ $? -eq 0 ]; then
	echo $(which apk) add # Alpine Linux
  else
	echo $(which apt-get) install # Debian based linux distros
  fi
}

function install {
  which $1 &> /dev/null

  if [ $? -ne 0 ]; then
    echo "Installing: ${1}..."
    $(get_package_manager) $1
  else
    echo "Already installed: ${1}"
  fi
}

function install_git_go_tools {
    go install github.com/$1@$2
}

install git
install vim
install tmux
install ranger
install go
install newsboat
install aerc

install_git_go_tools tomnomnom/gf latest
install_git_go_tools tomnomnom/gron latest
install_git_go_tools tomnomnom/assetfinder latest
install_git_go_tools tomnomnom/httprobe latest

